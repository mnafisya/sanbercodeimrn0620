console.log(' ')
console.log('======== Soal 1-Range ========')
console.log(' ')


function range( startnum,finishNum){
    if(!startnum||!finishNum){
return -1;
    }
    var deret = [];
if(startnum<finishNum){
    for (var i = startnum; i <= finishNum+1; i++){
    deret.push(i);
}}
else {if(startnum>finishNum){
for (var i = startnum; i >= finishNum+1; i--)
    deret.push(i);}
}
return deret
}
console.log(range(1, 10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54, 50));
console.log(range());

console.log(' ')
console.log('======== Soal 2-Range with Step========')
console.log(' ')

function rangeWithStep( startnum,finishNum, step){
    var deret = [];
if(startnum<finishNum){
    for (var i = startnum; i < finishNum+1; i += step){
    deret.push(i);
}}
else {if(startnum>finishNum){
for (var i = startnum; i >= finishNum; i -= step)
    deret.push(i);}
}
return deret
}

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

console.log(' ')
console.log('======== Soal 3-Sum of Range========')
console.log(' ')

function sum( startnum,finishNum, step){
    var result = 0;
    var numArray = [1]
if(step === undefined){
    numArray = range(startnum, finishNum)
}
else {
    numArray= rangeWithStep(startnum, finishNum)
}

for (var i = 0; i < numArray.length; i++){
    result += numArray [i]
}
return result
}

console.log(sum(1,10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15,10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0 


console.log(' ')
console.log('======== Soal 4-Multidimensi========')
console.log(' ')

var input = [
    ["0001"], ["Roman Alamsyah"], ["Bandar Lampung"], ["21/05/1989"], ["Membaca"],
    ["0002"], ["Dika Sembiring"], ["Medan"], ["10/10/1992"], ["Bermain Gitar"],
    ["0003"], ["Winona"], ["Ambon"], ["25/12/1965"], ["Memasak"],
    ["0004"], ["Bintang Senjaya"], ["Martapura"], ["6/4/1970"], ["Berkebun"]
] 
var keterangan = [
    ["Nomor Id : "], ["Nama : "], ["TTL : "], ["Membaca : "],
    ["Nomor Id : "], ["Nama : "], ["TTL : "], ["Membaca : "],
    ["Nomor Id : "], ["Nama : "], ["TTL : "], ["Membaca : "],
    ["Nomor Id : "], ["Nama : "], ["TTL : "], ["Membaca : "],
    ["Nomor Id : "], ["Nama : "], ["TTL : "], ["Membaca : "],] 
function datahandling(){
var nama = []   
for (var i = 0; i < input.length; i++){
    nama[i] = keterangan[i] + input [i];
    nama.push(i)
}
    return nama
}
console.log(datahandling());

console.log(' ')
console.log('======== Soal 5-Balik Kata========')
console.log(' ')



function balikKata(str) {
    var newString = "";
    for (var i = str.length - 1; i >= 0; i--) {
        newString += str[i];
    }
    return newString;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log(' ')
console.log('======== Soal 6-Metode Array========')
console.log(' ')


var dataHandling2 = ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"] 
dataHandling2.splice(1, 4, "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Pria") 
console.log(dataHandling2)

var pisah = dataHandling2[3].split("/")
desc = pisah.sort().reverse();
console.log(desc)

var tanggal2 = 05;
switch(tanggal2) {
  case 01:   { console.log('Januari'); break; }
  case 02:   { console.log('Februari'); break; }
  case 03:   { console.log('Maret'); break; }
  case 04:   { console.log('April'); break; }
  case 05:   { console.log('Mei'); break; }
  case 06:   { console.log('Juni'); break; }
  case 07:   { console.log('Juli'); break; }
  case 08:   { console.log('Agustus'); break; }
  case 09:   { console.log('September'); break; }
  case 10:   { console.log('OKtober'); break; }
  case 11:   { console.log('November'); break; }
  case 12:   { console.log('Desember'); break; }}




var slug = pisah.join(" - ");
console.log(slug); 
  
  
console.log(dataHandling2[1].toString());
var irisan = dataHandling2[1].slice(0,14)