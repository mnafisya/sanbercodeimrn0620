console.log('======== Soal 1-While loop ========')
console.log(' ')

var deret = 0;
console.log('Looping Pertama')
while(deret < 20) {
  deret += 2
  console.log(deret, '- I love coding')
}
var deret = 22
console.log('Looping Kedua')
while(deret > 0){
  deret -= 2
  console.log(deret, '- I will become a mobile developer')
}

console.log(' ')
console.log('======== Soal 2-For loop ========')
console.log(' ')

console.log('output')
for(var deret = 0; deret < 21; deret += 1)
if (deret%2 == 0){console.log(deret,'- Berkualitas');
}
else {if(deret%2 != 0){console.log(deret,'- Santai');}
  if(deret%3 == 0 && deret%2 != 0){console.log(deret,'- I Love Coding')}}

console.log(' ')
console.log('======== Soal 3-Membuat Persegi Panjang # ========')
console.log(' ')

var rows = 8
var cols = 4
for (var i = 0; i <cols; i++){
  var d = "";
  for(var j = 0; j<rows;j++){
    d=d+"#"
  }
  console.log(d);
}


console.log(' ')
console.log('======== Soal 4-Membuat Segitiga # ========')
console.log(' ')

var x, y;
x = y = "";

for (var row = 1; row <= 7; row++) {
  x += "#";
  y += x + "\n";
}

console.log(y);

console.log(' ')
console.log('======== Soal 5-Membuat Papan Catur # ========')
console.log(' ')


var rows = 8;
var cols = 8;
var board = "";

for (var y = 0; y < cols; y++) {
    if(y>0) board += "\n";
    for (var x = 0; x < rows; x++) {
        if ((x + y) % 2 == 0){
            board += " ";
        } else {
            board += "#";
        }
    }
}

console.log(board);